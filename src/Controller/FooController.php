<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Routing\Annotation\Route;

class FooController extends AbstractController {
	/**
	 * @var Swift_Mailer
	 */
	protected $mailer;

	public function __construct(Swift_Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * @Route("/test", methods={"POST"}, name="test")
	 */
	public function test(Request $request):Response{

		$message = new Swift_Message("Test Message");
		$message->setBody("Test", "text/plain");
		$message->setFrom("info@arimaclanka.com");
		$message->setTo("ramesh@arimaclanka.com");

		$response =  $this->mailer->send($message);

		return $this->json([
			"success" => true,
		], 200);
	}
}

