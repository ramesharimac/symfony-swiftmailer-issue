FROM registry.gitlab.com/arimac-au-bluemountain-iot/iot-server:php7.4-nginx


WORKDIR /var/www/html
COPY . /var/www/html

RUN composer install
