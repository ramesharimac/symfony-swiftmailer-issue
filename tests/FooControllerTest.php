<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FooControllerTest extends WebTestCase {
	public function testFoo(){
		static::ensureKernelShutdown();
		$client= self::createClient();
		$client->enableProfiler();
		$client->request('POST',
			'/test',
			[],
			[],
			['CONTENT_TYPE' => 'application/json'],
			''
		);

		$mailCollector = $client->getProfile()->getCollector('swiftmailer');
		$this->assertSame(1, $mailCollector->getMessageCount());
	}

}

?>
